void main(List<String> args) {
  //exercise1();
  //exersice2();
  //exersice3();
  //exersice4();
  //exersice5();
  //exersice6();
  //exersice7();
  //exersice8();
}

exercise1() {
  print('if, else method');
  int finger = 2;
  if (finger == 1) {
    print('first big finger');
  } else if (finger == 2) {
    print('second index finger');
  } else if (finger == 3) {
    print('thirty midlle finger');
  } else if (finger == 4) {
    print('forty ring finger');
  } else if (finger == 5) {
    print('five little finger');
  } else {
    print('error this command');
  }
  print('switch case method');

  String result = '';
  switch (finger) {
    case 1:
      {
        result = 'big finger';
      }
      break;
    case 2:
      {
        result = 'index finger';
      }
      break;
    case 3:
      {
        result = 'middle finger';
      }
      break;
    case 4:
      {
        result = 'ring finger';
      }
      break;
    case 5:
      {
        result = 'little finger';
      }
      break;
    default:
      {
        print('error this command');
      }
  }
  print(result);
}

exersice2() {
  int min = 59;
  if (min >= 1 && min <= 15) {
    print('first quarter');
  } else if (min >= 16 && min <= 30) {
    print('second quarter');
  } else if (min >= 31 && min <= 45) {
    print('thirty quarter');
  } else if (min >= 46 && min <= 59) {
    print('fourth quarter');
  } else {
    print('not quarter');
  }
}

exersice3() {
  String lang = 'eng';
  List<String> arr = [];
  const List<String> ru = [
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
    "Воскресенье",
  ];
  const List<String> eng = [
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];

  if (lang == 'ru') {
    arr = ru;
  } else if (lang == 'eng') {
    arr = eng;
  }
  print(arr);
}

exersice4() {
  String words = 'abcde';
  String check = words.substring(0, 1);
  if (check == 'a') {
    print('yes this word');
  } else {
    print('error this command');
  }
}

exersice5() {
  int num = 4;
  String result = '';
  switch (num) {
    case 1:
      {
        result = 'season: winter';
      }
      break;
    case 2:
      {
        result = 'season: spring';
      }
      break;
    case 3:
      {
        result = 'season: summer';
      }
      break;
    case 4:
      {
        result = 'season: autumn';
      }
      break;
    default:
      {
        print('error this commannd');
      }
  }
  print(result);
}

exersice6() {
  int number = -1;
  if (number < 0) {
    print('true <0');
  } else {
    print('false >=0');
  }
}

exersice7() {
  String code = '123123';
  int num0 = int.parse(code[0]);
  int num1 = int.parse(code[1]);
  int num2 = int.parse(code[2]);
  int num3 = int.parse(code[3]);
  int num4 = int.parse(code[4]);
  int num5 = int.parse(code[5]);
  int first = num0 + num1 + num2;
  int second = num3 + num4 + num5;

  if (first == second) {
    print('yes true number equal');
  } else {
    print('no false didnt equal');
  }
}

exersice8() {
  print('if, else Method');
  String lights = 'Red';
  if (lights == 'Red') {
    print('Stop wait Turn Red Light');
  } else if (lights == 'Orange') {
    print('get ready to go Turn Orange Light');
  } else {
    print('go cross this green light cross');
  }

  print('switch case Method');
  switch (lights) {
    case 'Red':
      {
        print('Stop Turn Red');
      }
      break;
    case 'Orange':
      {
        print('Get ready Turn Orange');
      }
      break;
    default:
      {
        print('go cross this your light');
      }
  }
}
